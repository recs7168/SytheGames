$(document).ready(function() {
    $( "#sythe-games-logo" ).mouseover(function(){
        $(this).attr("src", "images/sythe-games-logo-hover.png");
    });

    $( "#sythe-games-logo" ).mouseout(function(){
        $(this).attr("src", "images/sythe-games-logo.png");
    });
});